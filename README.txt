door opener
can also turn on stairway lights

to install and run:
1. git clone this repo
2. install udev rules (etc/udev/rules.d/99-gpio.rules)
3. sudo udevadm control --reload
4. sudo udevadm trigger --subsystem-match=gpio
6. copy example config to config.json, adjust values
7. start the server using start.sh
8. make a get request to /open?pin=x&time=y (e.g. pin=0, time=1000)
9. (start on boot) add the following line to chip account's crontab (crontab -e):
@reboot /home/chip/chip-door-opener/start.sh
