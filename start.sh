#!/bin/bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

cd "$(dirname "$0")"
screen -dmS door-opener bash -c 'node index.js || exec bash'
