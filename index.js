'use strict';

const fs = require('fs');
const http = require('http');
const HttpDispatcher = require('httpdispatcher');
const Gpio = require('chip-gpio').Gpio;

const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
const dispatcher = new HttpDispatcher;

let busyGpios = [];
const allowedGpios = config.gpio && config.gpio.allowed || [];

function leadingZero(num) {
	if (num > 9) {
		return num;
	}
	
	return '0' + num;
}

function logger(line) {
	const dt = new Date;
	const args = [line];
	
	args.unshift(
		'[' +
		leadingZero(dt.getDate()) + '.' +
		leadingZero(dt.getMonth()) + '.' +
		(dt.getFullYear()) + ' ' +
		leadingZero(dt.getHours()) + ':' +
		leadingZero(dt.getMinutes()) + ':' +
		leadingZero(dt.getSeconds()) +
		']'
	);
	
	console.log.apply(console, args);
}

dispatcher.onGet('/open', function(req, res) {
    if (
        !('pin' in req.params) ||
        !('time' in req.params)
    ) {
        res.writeHead(422, {
            'Content-Type': 'application/json'
        });

        res.end(JSON.stringify({
            success: false,
            message: 'Missing parameters'
        }));
        
        return;
    }
    
    const gpioNum = parseInt(req.params.pin, 10);
    
    if (allowedGpios.indexOf(gpioNum) === -1) {
        res.writeHead(403, {
            'Content-Type': 'application/json'
        });

        res.end(JSON.stringify({
            success: false,
            message: 'GPIO number not allowed'
        }));
        
        return;
    }
    
    if (busyGpios.indexOf(gpioNum) > -1) {
        res.writeHead(503, {
            'Content-Type': 'application/json'
        });

        res.end(JSON.stringify({
            success: false,
            message: 'GPIO is busy, try again later'
        }));
        
        return;
    }
    
    busyGpios.push(gpioNum);
    
    res.writeHead(200, {
        'Content-Type': 'application/json'
    });

    res.end(JSON.stringify({
        success: true,
        message: 'OK'
    }));
    
    const timerInterval = parseInt(req.params.time, 10);
    
    const currGpio = new Gpio(gpioNum, 'out');
    currGpio.write(0);
    
    setTimeout(function() {
        currGpio.write(1);
        currGpio.unexport();
        
        const idx = busyGpios.indexOf(gpioNum);
        
        if (idx > -1) {
            busyGpios.splice(idx, 1);
        }
    }, timerInterval);
});

http.createServer(function(req, res) {
	const conn = req.connection;
	
	logger('HTTP client connected: ' + conn.remoteAddress + ':' + conn.remotePort);
	logger(req.method + ' ' + req.url);
	
	try {
		dispatcher.dispatch(req, res);
	}
	catch(err) {
		logger(err);
	}
}).listen(config.listen.port, config.listen.hostname, function() {
	logger('Server listening on: http://' + config.listen.hostname + ':' + config.listen.port);
});
